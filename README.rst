The script, remove_target, is a line processing tool that will 
remove a string from a file of your specification. The get_target 
procedure should be modified according to your needs. You can process
one file or an entire directory of files, as specified in
your command line arguments. 

The script is verbose and that information can
be piped into another file at the time of execution on the
command line; for example:

usage: python remove_target <arg> > results.txt.

You will need to edit the config directives in the script
between lines 29 and 35; depending on the target to be removed.

Example files:
--------------
You can see this script in action using the script and file in the 
'example' directory.

Author: John Merigliano | May 26, 2014 (Modified Aug. 13, 2014)
