#!/usr/bin/python

import sys
import string

# This is a line processing tool that will remove a string
# from a file of your specification. The get_target procedure
# should be modified according to your needs. You can process
# one file or an entire directory of files, as specified in
# target_files. The script is verbose and that information can
# be piped into another file at the time of execution on the
# command line; for example:
# usage: python remove_target <arg> > results.txt.
#
# Author: John Merigliano | May 26, 2014 (Modified Aug. 13, 2014)


# Find the target to be removed.
def get_target(page):
    '''
        Defines the target to be removed.
        Returns the entire target.
    '''

    # Configuration directives:
    # -------------------------
    # EDIT begin --------------
    # Define the core of the target
    core = 'YOUR TARGET'
    # index_begin is an integer from the beginning of the core to
    # the beginning of the target string.
    index_begin = 0
    # index_end is an integer from the beginning of the core to
    # the end of the target string.
    index_end = 0
    # ---------------- EDIT end
    # -------------------------
    # Define the beginning of the entire target.
    begin = string.find(page, core) - index_begin
    # Define the end of the entire target.
    end = string.find(page, core) + index_end
    # Define the entire target string.
    target = page[begin:end]

    return target


# Remove the target and report results or errors.
def remove_target(finput):
    ''' Extracts the target as defined in get_target(). '''
    # Flag to proceed:
    ok = False

    try:
        # Open the file.
        fin = open(finput, 'r')
        ok = True
    except Exception:
        print 'There is no such file-->[ERROR]:', finput

    # Proceed if the file is found.
    if ok:
        # Read the page.
        page = fin.read()
        # Find the target in the page.
        target = get_target(page)
        # Remove the target.
        if target != '':
            # Replace the bad code with ''
            new = page.replace(target, '')
            fin.close()
            # Report the results:
            print finput, '-->[REMOVED]:', target

            # Overwrite the files and close.
            fout = open(finput, 'w')
            fout.write(new)
            fout.close()
        else:
            print finput, '-->[NO TARGET FOUND]:'


def main(args):
    # Execute the script within a file system.
    # Initiate the list to store all of the file names:
    target_files = []
    # Store all of the files from the directory into the list.
    for name in args:
        target_files.append(name)

    # Run the procedure to remove the targets.
    for name in target_files:
        # Traverse the file(s) and remove the target(s).
        remove_target(name)
    print '[DONE]'


# Specify the target file(s).
args = sys.argv[1:]  # Exclude the name of the script.

if __name__ == '__main__':
    main(args)
